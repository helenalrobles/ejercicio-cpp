#include <iostream>
#include "Shapes.h"

using namespace std;

int main()
{
    cout<<"\n Let's create a square with sides' length = 2.";
    Square mySquare(2);
    mySquare.PrintPoints();

    cout<<"\n And now move it using vector (2,4).";
    Point deltaVector(2,4);
    mySquare.Move(deltaVector);
    mySquare.PrintPoints();


}
