#include "Shapes.h"
#include <iostream>

/*********************************************/
//class Shape

Shape::Shape()
{
    _insertionPoint.set_xy(0,0);
}

Shape::~Shape()
{

}

void Shape::Draw ()
{

}
void Shape::Move (Point deltaVector)
{

}

/*********************************************/
//class Point
Point::Point()
{
    _x = 0;
    _y = 0;
}

Point::Point(int x, int y)
{
    _x = x;
    _y = y;
}

Point::~Point()
{

}

void Point::set_x(int x) { _x = x; }
void Point::set_y(int y) { _y = y; }
void Point::set_xy(int x, int y) { _x = x; _y = y; }

int Point::get_x () { return _x; }
int Point::get_y () { return _y; }

Point& Point::operator= (const Point& pointIn)
{
    _x = pointIn._x;
    _y = pointIn._y;

    return *this;
}

Point Point::operator+ (const Point& pointIn)
{
    Point returnPoint;

    returnPoint._x = pointIn._x + _x;
    returnPoint._y = pointIn._y + _y;

    return returnPoint;
}


/*********************************************/
//class Circle

Circle::Circle()
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _centre = _insertionPoint;

    _radius = 0;
}

Circle::Circle(Point centre)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _centre = centre;

    _radius = 0;
}

Circle::Circle(Point centre, int radius)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _centre = centre;

    _radius = radius;
}

Circle::Circle(Point centre, int radius, Point insertion)
{
    _centre = centre;

    _radius = radius;

    _insertionPoint = insertion;
}

Circle::~Circle()
{

}

void Circle::Draw()
{
    /*Here we will define how to draw a circle.
    Singleton class designed to draw will take advantage of polymorphism: receiving a
    Shape class, it will call the proper Draw function depending on the instance. */
}

void Circle::Move(Point deltaVector)
{
    _insertionPoint = deltaVector + _insertionPoint;
    _centre = deltaVector + _centre;
}

/*********************************************/
//class Triangle

Triangle::Triangle()
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _point1 = _point2 = _point3 = _insertionPoint;
}

Triangle::Triangle(Point p1, Point p2, Point p3)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _point1 = p1;
    _point2 = p2;
    _point3 = p3;
}

Triangle::Triangle(Point p1, Point p2, Point p3, Point insertion)
{
    _point1 = p1;
    _point2 = p2;
    _point3 = p3;

    _insertionPoint = insertion;
}

Triangle::~Triangle()
{

}

void Triangle::Draw()
{
    /*Here we will define how to draw a triangle.
    Singleton class designed to draw will take advantage of polymorphism: receiving a
    Shape class, it will call the proper Draw function depending on the instance. */
}

void Triangle::Move(Point deltaVector)
{
    _insertionPoint = deltaVector + _insertionPoint;

    _point1 = deltaVector + _point1;
    _point2 = deltaVector + _point2;
    _point3 = deltaVector + _point3;
}

/*********************************************/
//class Square

Square::Square()
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _point1 = _point2 = _point3 = _point4 = _insertionPoint;
    _sideSize = 0;
}

Square::Square(int sideSize)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _sideSize = sideSize;

    this->SetPointsUsingLowerLeftPoint(_insertionPoint);
}

Square::Square(int sideSize, Point lowerLeftPoint)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _sideSize = sideSize;

    this->SetPointsUsingLowerLeftPoint(lowerLeftPoint);
}

Square::Square(int sideSize, Point lowerLeftPoint, Point insertion)
{
    _insertionPoint = insertion;
    _sideSize = sideSize;

    this->SetPointsUsingLowerLeftPoint(lowerLeftPoint);
}

Square::~Square()
{

}

void Square::SetPointsUsingLowerLeftPoint(Point lowerLeftPoint)
{
    _point4 = _point1 =_point3 = lowerLeftPoint;

    //lower right: it will have the same y value as lower left
    // but its x value will be lower left's plus side length
    _point3.set_x(_point4.get_x() + _sideSize);

    //upper left: it will have the same x value as lower left
    // but its y value will be lower left's plus side length
    _point1.set_y(_point4.get_y() + _sideSize);

    //upper right: it will have the same x value as lower right
    //and the same y value as upper left (both already calculated).
    _point2.set_x(_point3.get_x());
    _point2.set_y(_point1.get_y());
}


void Square::PrintPoints()
{
    std::cout<<"\n My upper left point is: ("<< _point1.get_x() << "," << _point1.get_y() << ")";
    std::cout<<"\n My upper right point is: ("<< _point2.get_x() << "," << _point2.get_y() << ")";
    std::cout<<"\n My lower right point is: ("<< _point3.get_x() << "," << _point3.get_y() << ")";
    std::cout<<"\n My lower left point is: ("<< _point4.get_x() << "," << _point4.get_y() << ")";
}

void Square::Draw ()
{
    /*Here we will define how to draw a square.
    Singleton class designed to draw will take advantage of polymorphism: receiving a
    Shape class, it will call the proper Draw function depending on the instance. */
}

void Square::Move (Point deltaVector)
{
    _insertionPoint = deltaVector + _insertionPoint;

    _point1 = deltaVector + _point1;
    _point2 = deltaVector + _point2;
    _point3 = deltaVector + _point3;
    _point4 = deltaVector + _point4;
}

/*********************************************/
//class Rectangle

Rectangle::Rectangle()
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _point1 = _point2 = _point3 = _point4 = _insertionPoint;

    _verticalSideSize = 0;
    _horizontalSideSize = 0;
}

Rectangle::Rectangle(Point lowerLeftPoint, int horizontalSideSize, int verticalSideSize)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _verticalSideSize = verticalSideSize;
    _horizontalSideSize = horizontalSideSize;

    this->SetPointsUsingLowerLeftPoint(lowerLeftPoint);
}

Rectangle::Rectangle(Point lowerLeftPoint, int horizontalSideSize, int verticalSideSize, Point insertion)
{
    _insertionPoint = insertion;

    _verticalSideSize = verticalSideSize;
    _horizontalSideSize = horizontalSideSize;

    this->SetPointsUsingLowerLeftPoint(lowerLeftPoint);
}

Rectangle::~Rectangle()
{

}

void Rectangle::SetPointsUsingLowerLeftPoint(Point lowerLeftPoint)
{
    _point4 = _point1 =_point3 = lowerLeftPoint;

    //lower right: it will have the same y value as lower left
    // but its x value will be lower left's plus horizontal side length
    _point3.set_x(_point4.get_x() + _horizontalSideSize);

    //upper left: it will have the same x value as lower left
    // but its y value will be lower left's plus vetical side length
    _point1.set_y(_point4.get_y() + _verticalSideSize);

    //upper right: it will have the same x value as lower right
    //and the same y value as upper left (both already calculated).
    _point2.set_x(_point3.get_x());
    _point2.set_y(_point1.get_y());
}

void Rectangle::Draw ()
{
     /*Here we will define how to draw a rectangle.
    Singleton class designed to draw will take advantage of polymorphism: receiving a
    Shape class, it will call the proper Draw function depending on the instance. */
}

void Rectangle::Move (Point deltaVector)
{
    _insertionPoint = deltaVector + _insertionPoint;

    _point1 = deltaVector + _point1;
    _point2 = deltaVector + _point2;
    _point3 = deltaVector + _point3;
    _point4 = deltaVector + _point4;
}

/*********************************************/
//class Segment

Segment::Segment()
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);
}

Segment::Segment(Point head, Point tail)
{
    _insertionPoint.set_x(0);
    _insertionPoint.set_y(0);

    _head = head;
    _tail = tail;
}
Segment::Segment(Point head, Point tail, Point insertion)
{
    _head = head;
    _tail = tail;
    _insertionPoint = insertion;
}

Segment::~Segment()
{

}

void Segment::Draw ()
{
     /*Here we will define how to draw a segment.
    Singleton class designed to draw will take advantage of polymorphism: receiving a
    Shape class, it will call the proper Draw function depending on the instance. */
}

void Segment::Move (Point deltaVector)
{
    _insertionPoint = deltaVector + _insertionPoint;
    _head = _head + deltaVector;
    _tail = _tail + deltaVector;
}
