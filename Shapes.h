/*********************************************/
class Point
{
private:
    int _x;
    int _y;

public:
    Point();
    Point(int x, int y);
    ~Point();

    void set_x(int x);
    void set_y(int y);
    void set_xy(int x, int y);

    int get_x ();
    int get_y ();

    Point& operator= (const Point& pointIn);

    Point operator+ (const Point& pointIn);
};

/*********************************************/

class Shape
{
protected:
    Point _insertionPoint;

public:
    Shape();
    virtual ~Shape();

    virtual void Draw ();
    virtual void Move (Point deltaVector);
};

/*********************************************/

class Circle : public Shape
{
private:
    Point _centre;
    int _radius;

public:
    Circle();
    Circle(Point centre);
    Circle(Point centre, int radius);
    Circle(Point centre, int radius, Point insertion);
    ~Circle();

    void Draw();
    void Move(Point deltaVector);

};

/*********************************************/

class Triangle : public Shape
{
private:
    Point _point1;
    Point _point2;
    Point _point3;

public:
    Triangle();
    Triangle(Point p1, Point p2, Point p3);
    Triangle(Point p1, Point p2, Point p3, Point insertion);
    ~Triangle();

    void Draw ();
    void Move (Point deltaVector);
};

/*********************************************/

class Square : public Shape
{
private:
    Point _point1; //Upper left
    Point _point2; //Upper right
    Point _point3; //Lower right
    Point _point4; //Lower left
    int _sideSize;

    void SetPointsUsingLowerLeftPoint(Point lowerLeftPoint);

public:
    Square();
    Square(int sideSize);
    Square(int sideSize, Point upperLeftPoint);
    Square(int sideSize, Point upperLeftPoint, Point insertion);
    ~Square();

    void PrintPoints();

    void Draw ();
    void Move (Point deltaVector);
};

/*********************************************/

class Rectangle : public Shape
{
private:
    Point _point1; //Upper left
    Point _point2; //Upper right
    Point _point3; //Lower right
    Point _point4; //Lower left
    int _horizontalSideSize;
    int _verticalSideSize;

    void SetPointsUsingLowerLeftPoint(Point lowerLeftPoint);

public:
    Rectangle();
    Rectangle(Point upperLeftPoint, int horizontalSideSize, int verticalSideSize);
    Rectangle(Point upperLeftPoint, int horizontalSideSize, int verticalSideSize, Point insertion);
    ~Rectangle();

    void Draw ();
    void Move (Point deltaVector);
};

/*********************************************/

class Segment : public Shape
{
private:
    Point _head;
    Point _tail;

public :
    Segment();
    Segment(Point head, Point tail);
    Segment(Point head, Point tail, Point insertion);
    ~Segment();

    void Draw ();
    void Move (Point deltaVector);
};


/*********************************************/

